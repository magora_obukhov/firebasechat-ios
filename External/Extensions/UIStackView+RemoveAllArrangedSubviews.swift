import UIKit

extension UIStackView {
    
    func removeAllArrangedSubviews() {
        for view in self.arrangedSubviews {
            view.removeFromSuperview()
        }
    }
}
