import UIKit

extension UIImage {
    
    func fixOrientation() -> UIImage {
        
        if ( self.imageOrientation == UIImageOrientation.up ) {
            return self;
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        if ( self.imageOrientation == UIImageOrientation.down || self.imageOrientation == UIImageOrientation.downMirrored ) {
            transform = transform.translatedBy(x: self.size.width, y: self.size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
        }
        
        if ( self.imageOrientation == UIImageOrientation.left || self.imageOrientation == UIImageOrientation.leftMirrored ) {
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi / 2.0))
        }
        
        if ( self.imageOrientation == UIImageOrientation.right || self.imageOrientation == UIImageOrientation.rightMirrored ) {
            transform = transform.translatedBy(x: 0, y: self.size.height);
            transform = transform.rotated(by: CGFloat(-Double.pi / 2.0));
        }
        
        if ( self.imageOrientation == UIImageOrientation.upMirrored || self.imageOrientation == UIImageOrientation.downMirrored ) {
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        }
        
        if ( self.imageOrientation == UIImageOrientation.leftMirrored || self.imageOrientation == UIImageOrientation.rightMirrored ) {
            transform = transform.translatedBy(x: self.size.height, y: 0);
            transform = transform.scaledBy(x: -1, y: 1);
        }
        
        let ctx: CGContext = CGContext(data: nil, width: Int(self.size.width), height: Int(self.size.height),
                                       bitsPerComponent: self.cgImage!.bitsPerComponent, bytesPerRow: 0,
                                       space: self.cgImage!.colorSpace!,
                                       bitmapInfo: self.cgImage!.bitmapInfo.rawValue)!;
        
        ctx.concatenate(transform)
        
        if ( self.imageOrientation == UIImageOrientation.left ||
            self.imageOrientation == UIImageOrientation.leftMirrored ||
            self.imageOrientation == UIImageOrientation.right ||
            self.imageOrientation == UIImageOrientation.rightMirrored ) {
            ctx.draw(self.cgImage!, in: CGRect(x: 0,y: 0,width: self.size.height,height: self.size.width))
        } else {
            ctx.draw(self.cgImage!, in: CGRect(x: 0,y: 0,width: self.size.width,height: self.size.height))
        }
        
        return UIImage(cgImage: ctx.makeImage()!)
    }
    
    func shrinkToFullHD() -> UIImage {
        let fullHDSize = CGSize(width:1920.0, height:1080.0)
        if ( self.size.width > fullHDSize.width ||
           self.size.height > fullHDSize.height ) {
                let horizontalRatio = fullHDSize.width / size.width
                let verticalRatio = fullHDSize.height / size.height
                
                let ratio = min(horizontalRatio, verticalRatio)
                let newSize = CGSize(width: size.width * ratio, height: size.height * ratio)
                UIGraphicsBeginImageContextWithOptions(newSize, true, 0)
                draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
                let newImage = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                return newImage!
        } else {
            return self
        }
    }
    
    func shrinkToSize(resultSize: CGSize) -> UIImage {
            UIGraphicsBeginImageContextWithOptions(resultSize, true, 0)
            draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: resultSize))
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return newImage!
    }
    
    func square() -> UIImage {
        let sideLength = min(self.size.width, self.size.height)
        var rect = CGRect(x: (self.size.width - sideLength)/2.0, y: (self.size.height - sideLength)/2.0, width: sideLength, height: sideLength)
        rect.origin.x *= self.scale
        rect.origin.y *= self.scale
        rect.size.width *= self.scale
        rect.size.height *= self.scale
        let imageRef = self.cgImage!.cropping(to: rect)
        let image = UIImage(cgImage: imageRef!, scale: self.scale, orientation: self.imageOrientation)
        return image
    }
    
    func blur() -> UIImage {
        let image = self
        let context = CIContext(options: nil)
        let inputImage = CIImage(image: image)
        let originalOrientation = image.imageOrientation
        let originalScale = image.scale
        let filter = CIFilter(name: "CIGaussianBlur")
        filter?.setValue(inputImage, forKey: kCIInputImageKey)
        filter?.setValue(55.0, forKey: kCIInputRadiusKey)
        let outputImage = filter?.outputImage
        var cgImage:CGImage?
        if let outputImage = outputImage {
            cgImage = context.createCGImage(outputImage, from: (inputImage?.extent)!)
        }
        if let cgImage = cgImage {
            return UIImage(cgImage: cgImage, scale: originalScale, orientation: originalOrientation)
        }
        return image
    }
    
}

//extension UIImage {
//    func getPixelColor(pos: CGPoint) -> UIColor {
//        
//        let provider = self.cgImage!.dataProvider
//        let providerData = provider!.data
//        let data = CFDataGetBytePtr(providerData)
//        
//        let stringTag = 0
//        let numberOfComponents = 4
//        let pixelData = (((Int(size.width) + stringTag) * Int(pos.y)) + Int(pos.x)) * numberOfComponents
//        
//        let r = CGFloat((data?[pixelData])!) / 255.0
//        let g = CGFloat((data?[pixelData + 1])!) / 255.0
//        let b = CGFloat((data?[pixelData + 2])!) / 255.0
//        let a = CGFloat((data?[pixelData + 3])!) / 255.0
//        //return UIColor(red: r, green: g, blue: b, alpha: 1)
//        return UIColor(red: r, green: g, blue: b, alpha: a)
//    }
//}

extension UIImage {
    
    func getPixelColor(point: CGPoint) -> UIColor {
        let cgImage = self.cgImage
        let width = Int(size.width)
        let height = Int(size.height)
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        let context = CGContext(data: nil, width: width, height: height, bitsPerComponent: 8, bytesPerRow: width * 4, space: colorSpace, bitmapInfo: CGBitmapInfo.byteOrder32Little.rawValue | CGImageAlphaInfo.premultipliedFirst.rawValue)!
//        CGContextDrawImage(context, CGRect(x: 0, y: 0, width: size.width, height: size.height), cgImage)
        
        context.draw(cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        
        let pixelBuffer = context.data?.assumingMemoryBound(to: UInt32.self)
        let pixel = pixelBuffer! + Int(point.y) * width + Int(point.x)
        
        let r = CGFloat(red(color: pixel.pointee))   / CGFloat(255.0)
        let g = CGFloat(green(color: pixel.pointee)) / CGFloat(255.0)
        let b = CGFloat(blue(color: pixel.pointee))  / CGFloat(255.0)
        let a = CGFloat(alpha(color: pixel.pointee)) / CGFloat(255.0)
        
        return UIColor(red: r, green: g, blue: b, alpha: a)
    }
    
    private func alpha(color: UInt32) -> UInt8 {
        return UInt8((color >> 24) & 255)
    }
    
    private func red(color: UInt32) -> UInt8 {
        return UInt8((color >> 16) & 255)
    }
    
    private func green(color: UInt32) -> UInt8 {
        return UInt8((color >> 8) & 255)
    }
    
    private func blue(color: UInt32) -> UInt8 {
        return UInt8((color >> 0) & 255)
    }
    
//    private func rgba(red red: UInt8, green: UInt8, blue: UInt8, alpha: UInt8) -> UInt32 {
//        return (UInt32(alpha) << 24) | (UInt32(red) << 16) | (UInt32(green) << 8) | (UInt32(blue) << 0)
//    }
    
}

extension UIImage {
    
    func drawSquare(point: CGPoint, size: CGSize, color: CGColor, context: CGContext) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.size, false, 0.0)
        
        self.draw(in: CGRect(origin: CGPoint.zero, size: self.size))
 
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color)
        context?.setStrokeColor(color)
        context?.setLineWidth(1)
        
        let rectangle = CGRect(origin: point, size: size)
        context?.addRect(rectangle)
        context?.drawPath(using: .fillStroke)

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func startPixelate() {
        UIGraphicsBeginImageContextWithOptions(self.size, false, 0.0)
        self.draw(in: CGRect(origin: CGPoint.zero, size: self.size))
        
    }
    
    func restartPixelate() {
        UIGraphicsEndImageContext()
        self.startPixelate()
    }
    
    func pixelateSquare(indexPath: IndexPath, maxIndexPath: IndexPath, color: CGColor) -> UIImage? {
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color)
        context?.setStrokeColor(color)
        let lineWidth:CGFloat = 1.0
        context?.setLineWidth(lineWidth)
        
        let cellWidth = self.size.width / CGFloat(maxIndexPath.row + 1)
        let cellHeight = self.size.height / CGFloat(maxIndexPath.section + 1)
        
        let squareCellSide = max(cellWidth, cellHeight)
        
        let touchSquareSide = max(self.size.width, self.size.height)
        let widthShift = (touchSquareSide - self.size.width) / 2.0
        let heightShift = (touchSquareSide - self.size.height) / 2.0
        print(indexPath.section)
        print(indexPath.row)
        print(cellHeight)
        print(heightShift)
        print(CGFloat(indexPath.section) * squareCellSide - widthShift)
        print(CGFloat(indexPath.row) * squareCellSide - heightShift)
        
        let origin = CGPoint(x:CGFloat(indexPath.section) * squareCellSide - widthShift , y:CGFloat(indexPath.row) * squareCellSide - heightShift)
        let size = CGSize(width: squareCellSide - lineWidth, height: squareCellSide - lineWidth)
        let rectangle = CGRect(origin: origin, size: size)
        context?.addRect(rectangle)
        context?.drawPath(using: .fillStroke)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        return newImage
    }
}
