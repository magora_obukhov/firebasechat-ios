import Foundation

extension String {

    func matchBy(regex: String) -> Bool {
        let regExp = try! NSRegularExpression(pattern: regex, options: .caseInsensitive)
        return regExp.numberOfMatches(in: self, range: NSMakeRange(0, self.count)) > 0
    }
}
//
//extension String {
//    func nsRange(from range: Range<String.Index>) -> NSRange {
//        let from = range.lowerBound.samePosition(in: utf16)
//        let to = range.upperBound.samePosition(in: utf16)
//        return NSRange(location: utf16.distance(from: utf16.startIndex, to: from),
//                       length: utf16.distance(from: from, to: to))
//    }
//}

extension String {
    func range(from nsRange: NSRange) -> Range<String.Index>? {
        guard
            let from16 = utf16.index(utf16.startIndex, offsetBy: nsRange.location, limitedBy: utf16.endIndex),
            let to16 = utf16.index(utf16.startIndex, offsetBy: nsRange.location + nsRange.length, limitedBy: utf16.endIndex),
            let from = from16.samePosition(in: self),
            let to = to16.samePosition(in: self)
            else { return nil }
        return from ..< to
    }
}


extension String {
    func short(int: Int) -> String {
        var tempFloat = Float(int)
        var exp = 0
        while tempFloat > 1000 {
            tempFloat = tempFloat / 1000
            exp += 3
        }
        var expTag = "E+" + String(exp)
        switch exp {
        case 0:
            expTag = ""
            break
        case 3:
            expTag = "K"
            break
        case 6:
            expTag = "M"
            break
        case 9:
            expTag = "B"
            break
        case 12:
            expTag = "T"
            break
        case 15:
            expTag = "Q"
            break
        default:
            break
        }
        if exp == 0 {
           return String(format: "%.0f",tempFloat) + expTag
        } else {
          return String(format: "%.1f",tempFloat) + expTag
        }
    }
}
