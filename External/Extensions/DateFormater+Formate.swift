import Foundation

extension DateFormatter {
    
    static let instanse = DateFormatter()
    
    static var `default`: DateFormatter {
        get {
            let df = instanse
            df.dateFormat = "dd.MM.yyyy"
            return df
        }
    }
}

