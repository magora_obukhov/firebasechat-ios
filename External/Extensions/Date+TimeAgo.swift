import Foundation

//extension Date {
//
//    func stringTimeAgo() -> String {
//        let calendar = Calendar.current
//        let now = Date()
//        let earliest = (now as NSDate).earlierDate(self)
//        let latest = (earliest == now) ? self : now
//        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour, NSCalendar.Unit.day, NSCalendar.Unit.month, NSCalendar.Unit.year, NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
//        if (components.year! >= 1) {
//            let formatter = DateFormatter()
//            formatter.dateFormat = "dd.MM.yyyy"
//            return formatter.string(from: self)
//        } else if (components.month! >= 1) || (components.day! >= 8) {
//            let formatter = DateFormatter()
//            formatter.dateFormat = "dd.MM"
//            return formatter.string(from: self)
//        } else {
//            if (components.day! >= 2) {
//                return L10n.Timestamp.daysAgo(components.day!)
//            } else if (components.day! >= 1){
//                return L10n.Timestamp.oneDayAgo
//            } else if (components.hour! >= 2) {
//                return L10n.Timestamp.hoursAgo(components.hour!)
//            } else if (components.hour! >= 1){
//                return L10n.Timestamp.oneHourAgo
//            } else if (components.minute! >= 2) {
//                return L10n.Timestamp.minutesAgo(components.minute!)
//            } else if (components.minute! >= 1){
//                return L10n.Timestamp.oneMinuteAgo
//            } else {
//                return L10n.Timestamp.justNow
//            }
//        }
//    }
//}

extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    var endOfDay: Date {
        return Calendar.current.startOfDay(for: self).addingTimeInterval((60 * 60 * 24) - 1)
    }
}

extension Date {
    var startOfMonth: Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    var endOfMonth: Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth)!
    }
}
