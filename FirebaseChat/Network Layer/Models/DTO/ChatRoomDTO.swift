import Foundation
import ObjectMapper

struct ChatRoomDTO: Mappable {
    
    var id: String?
    var name: String?
    
    init?(map: Map) {
    }
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    mutating func mapping(map: Map) {
        self.name <- map["name"]

    }
}
