import Foundation
import Firebase

extension DatabaseReference {
    //Root
    var rooms: DatabaseReference { return self.child("rooms") }
    var list: DatabaseReference { return self.child("list") }
    var users: DatabaseReference { return self.child("users") }
    var members: DatabaseReference { return self.child("members") }
    var messages: DatabaseReference { return self.child("messages") }
    
    func byId(_ id: String) -> DatabaseReference { return self.child(id) }
    
    var text: DatabaseReference { return self.child("text") }
    var userID: DatabaseReference { return self.child("userID") }
}

//class DatabaseReferenceDecorator: NSObject {
//    fileprivate var decorated: DatabaseReference
//
//    required init(_ decorated: DatabaseReference) {
//        self.decorated = decorated
//        super.init()
//    }
//
//    override func forwardingTarget(for aSelector: Selector!) -> Any? {
//        return self.decorated
//    }
//}
//
//class RoomsRef: DatabaseReferenceDecorator {
//    func byId(_ id: String) -> RoomRef { return RoomRef(decorated.child(id)) }
//}
//
//class RoomRef: DatabaseReferenceDecorator {
//    var messages: MessagesRef { return MessagesRef(decorated.child("messages")) }
//}
//
//class MessagesRef: DatabaseReferenceDecorator {
//    func byId(_ id: String) -> MessageRef { return MessageRef(decorated.child(id)) }
//}
//
//class MessageRef: DatabaseReferenceDecorator {
//    var text: DatabaseReference { return decorated.child("text") }
//    var userID: DatabaseReference { return decorated.child("userID") }
//}
//
//class Users: DatabaseReferenceDecorator {
//    var text: DatabaseReference { return decorated.child("text") }
//    var userID: DatabaseReference { return decorated.child("userID") }
//}

//extension DatabaseReference {
//    var users: DatabaseReference { return self.child("users") }
//    var rooms: RoomsRef { return RoomsRef(self.child("rooms")) }
//}

/////

//protocol RoomsRef <T>{
//    func byId<T: DatabaseReference & RoomRef>(_ id: String) -> T
//}
//
//extension RoomsRef where Self: DatabaseReference{
//    //func byId(_ id: String) -> RoomRef { return self.child(id) as! RoomRef}
//    func byId<T: DatabaseReference & RoomRef>(_ id: String) -> T { return self.child(id) as! T}
//}
//
//protocol RoomRef {
//    var messages: MessagesRef { get }
//}
//
//extension RoomRef where Self: DatabaseReference{
//    var messages: MessagesRef { return self.child("messages") as! MessagesRef}
//}
//
//protocol MessagesRef {
//    func byId(_ id: String) -> MessageRef
//}
//
//extension MessagesRef where Self: DatabaseReference{
//    func byId(_ id: String) -> MessageRef { return self.child(id) as! MessageRef }
//}
//
//protocol MessageRef {
//    var text: DatabaseReference { get }
//    var userID: DatabaseReference { get }
//}
//
//extension MessageRef where Self: DatabaseReference{
//    var text: DatabaseReference { return self.child("text") }
//    var userID: DatabaseReference { return self.child("userID") }
//}
//
//extension DatabaseReference {
//    var users: DatabaseReference { return self.child("users") }
//    func rooms<R: DatabaseReference & RoomsRef>() -> R { return self.child("rooms") as! RoomsRef<DatabaseReference> }
//}
