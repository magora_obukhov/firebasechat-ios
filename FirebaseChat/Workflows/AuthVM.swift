import Foundation
import Firebase

protocol AuthVMOutput {
    var finishedWithSuccess: (() -> Void)? { get set }
    var finishedWithError: ((_ error: Error) -> Void)? { get set }
}

protocol AuthVMInput {
    func didSignInWith (authDataResult: AuthDataResult?, error: Error?)
}

class AuthVMImpl:  AuthVMInput, AuthVMOutput{
    //MARK: - LoginCompleted
    var finishedWithSuccess: (() -> Void)?
    var finishedWithError: ((_ error: Error) -> Void)?
    private let context: AuthServiceContext
    
    // MARK: - Lifecycle
    init(context: AuthServiceContext) {
        self.context = context
    }
    
    func didSignInWith (authDataResult: AuthDataResult?, error: Error?) {
        if let error = error, let finishedWithError = self.finishedWithError {
            finishedWithError(error)
        } else if let finishedWithSuccess = self.finishedWithSuccess{
            finishedWithSuccess()
        }
    }
}
