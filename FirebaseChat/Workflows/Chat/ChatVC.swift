import Foundation
import MAGChatUI

class ChatVC: MAGChatViewController {
    //MARK: - Properties
    var viewModel: ChatRoomsVM!
    
    var chatService = ChatService()
    private let person1 = MAGChatPerson()
    private var messageCount = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        person1.name = "User one"
        chatService.newMessageHandler = {(newMessage) in
            self.messageCount += 1
            let message = MAGChatMessage()
            message.messageText = newMessage
            message.sender = self.person1
            message.identifier = String(self.messageCount)
            message.serverTimeStamp = Date.init(timeIntervalSinceNow: 0)
            message.receivedTimeStamp = Date.init(timeIntervalSinceNow: 0)
            message.senderTimeStamp = Date.init(timeIntervalSinceNow: 0)
            message.unRead = true;
            self.update(message)
        }
    }
}

extension ChatVC: MAGChatViewControllerDelegate {
    func needMessagesOffset(by message: MAGChatMessage!) {
        
    }
    
    func messageDidCreate(_ message: MAGChatMessage!) {
        self.chatService.sendMessage(text: message.messageText)
    }
}
