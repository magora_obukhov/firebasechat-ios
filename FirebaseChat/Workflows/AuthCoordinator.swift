import UIKit


class AuthCoordinator: BaseCoordinator {
    //MARK: - Properties
    let router: UINavigationController
    let context: AppContext
    
    
    //MARK: - Lifecycle
    init(router: UINavigationController, context: AppContext) {
        self.router = router
        self.context = context
        router.setTranslucent()
    }
    
    //MARK: - Flows
    override func start() {

        let vm = AuthVMImpl(context: self.context)
        vm.finishedWithError = {error in
            Log("Error occured: \(error)")
        }

        vm.finishedWithSuccess = { [weak self] in
            Log("Successfully authorized!")
            self?.completionHandler?()
        }
        let vc = AuthVC()
        vc.viewModel = vm
        router.pushViewController(vc, animated: false)
    }
}

