import Foundation
import RxSwift

class ChatRoomsVM {
    let rooms = Variable<[ChatRoom]>([])
    private let context: AppContext
    private let disposeBag = DisposeBag()
    
    // MARK: - Lifecycle
    init(context: AppContext) {
        self.context = context
        
        self.context.chatService.allRoomsObserver()
            .subscribe(onNext: {[weak self] (rooms) in
                self?.rooms.value = rooms
            })
            .disposed(by: self.disposeBag)
    }
}
