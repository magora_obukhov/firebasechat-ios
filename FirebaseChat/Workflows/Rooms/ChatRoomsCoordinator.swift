import UIKit

class ChatRoomsCoordinator: BaseCoordinator {
    //MARK: - Properties
    let router: UINavigationController
    let context: AppContext
    
    
    //MARK: - Lifecycle
    init(router: UINavigationController, context: AppContext) {
        self.router = router
        self.context = context
        router.setTranslucent()
    }
    
    //MARK: - Flows
    override func start() {
        let vm = ChatRoomsVM(context: self.context)
        let vc = ChatRoomsVC()
        vc.viewModel = vm
        router.pushViewController(vc, animated: false)
    }
}
