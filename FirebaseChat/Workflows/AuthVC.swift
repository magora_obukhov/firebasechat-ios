import UIKit
import FirebaseAuthUI

class AuthVC: UIViewController {
    
    //MARK: - Properties
    var viewModel: AuthVMInput!
    var authUI: FUIAuth!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.authUI = FUIAuth.defaultAuthUI()
        self.authUI.delegate = self
        let authVC = self.authUI.authViewController()
        authVC.navigationItem.leftBarButtonItem = nil
        self.present(authVC, animated: false);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension AuthVC: FUIAuthDelegate {
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        self.viewModel.didSignInWith(authDataResult: authDataResult, error: error)
    }
}

extension FUIAuthBaseViewController{
    open override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = .red
    }
}
