import Foundation
import Firebase

protocol AuthService {
    var isAuthorized: Bool { get }
}

class AuthServiceImpl: AuthService {
    var isAuthorized: Bool { return Auth.auth().currentUser != nil }
    
    init() {
    }
}
