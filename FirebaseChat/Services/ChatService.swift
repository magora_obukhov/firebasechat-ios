import Foundation
import Firebase
import RxSwift
import ObjectMapper

class ChatService {
    var chatRooms = Variable<String>("")
    
    private var ref: DatabaseReference!
    private var observedNewMessageHandle: UInt?
    private let roomID = "room_test_id"
    
    var newMessageHandler: ((String) -> Void)! {
        willSet {
            if let handle = self.observedNewMessageHandle {
                ref.removeObserver(withHandle: handle)
            }
            self.observedNewMessageHandle = ref.rooms.messages.byId(roomID).observe(.childAdded, with: { (snapshot) in
                if let newMessage = snapshot.value as? String {
                    newValue(newMessage)
                }
            })
        }
    }
    
    init() {
        ref = Database.database().reference()
    }
    
    func sendMessage(text: String) {
        ref.rooms.messages.byId(roomID).childByAutoId().setValue(text)
    }
    
    func create(roomNamed roomName: String) -> Single<ChatRoom>{
        return self.roomNamed(roomName)
            .map({ (foundRoom) in
                if foundRoom == nil {
                    //Create new room only if there is no existing one
                    let roomID = self.ref.rooms.list.childByAutoId().key
                    let roomToCreate = ChatRoomDTO(id: roomID, name: roomName)
                    self.ref.rooms.list.child(roomID).setValue(roomToCreate.toJSON())
                    return ChatRoom(dto: roomToCreate)
                }
                return foundRoom!
            })
    }
    
    func roomNamed(_ nameToFound: String) -> Single<ChatRoom?> {
        return self.allRooms().map({ (allRooms) in
            for room in allRooms {
                if room.name == nameToFound {
                    return room
                }
            }
            return nil
        })
    }
    
    func allRooms() -> Single<[ChatRoom]> {
        return Single<[ChatRoom]>.create(subscribe: {[weak self] (single) -> Disposable in
            self?.ref.rooms.list.observeSingleEvent(of: .value) { (snapshot) in
                var allRooms = [ChatRoom]()
                if let roomsDict = snapshot.value as? [String: [String: Any]] {
                    for (roomID, roomDict) in roomsDict {
                        if var roomFound = ChatRoomDTO(JSON: roomDict) {
                            roomFound.id = roomID
                            allRooms.append(ChatRoom(dto: roomFound))
                        }
                    }
                }
                single(.success(allRooms))
            }
            return Disposables.create()
        })
    }
    
    enum DatebaseEvent {
        case added
        case deleted
        case modified
    }
    
    func addedRoomsObserver() -> Observable<ChatRoom> {
        return Observable<ChatRoom>.create({[weak self] (subscriber) -> Disposable in
            let handle: UInt! = self?.ref.rooms.list.observe(DataEventType.childRemoved, with: { (snapshot) in
                if let roomDict = snapshot.value as? [String: Any] {
                    if var roomDTO = ChatRoomDTO(JSON: roomDict) {
                        roomDTO.id = snapshot.key
                        subscriber.onNext(ChatRoom(dto: roomDTO))
                    }
                }
            })

            return Disposables.create {
                self?.ref.removeObserver(withHandle: handle)
            }
        })
    }
    
    func allRoomsObserver() -> Observable<[ChatRoom]> {
        return Observable<[ChatRoom]>.create({[weak self] (subscriber) -> Disposable in
            let handle: UInt! = self?.ref.rooms.list.observe(.value) { (snapshot) in
                var allRooms = [ChatRoom]()
                if let roomsDict = snapshot.value as? [String: [String: Any]] {
                    for (roomID, roomDict) in roomsDict {
                        if var roomFound = ChatRoomDTO(JSON: roomDict) {
                            roomFound.id = roomID
                            allRooms.append(ChatRoom(dto: roomFound))
                        }
                    }
                }
                subscriber.onNext(allRooms)
            }
            return Disposables.create {
                self?.ref.removeObserver(withHandle: handle)
            }
        })
    }
}
