import UIKit

class AppCoordinator: BaseCoordinator {
    //MARK: - Properties
    let window: UIWindow
    var context: AppContext
    private var authCoordinator: AuthCoordinator?
    private var chatRoomsCoordinator: ChatRoomsCoordinator?
    
    //MARK: - Lifecycle
    init(window: UIWindow, context: AppContext) {
        self.window = window
        self.context = context
    }
    
    //MARK: - Flows
    override func start() {
        chooseFlow()
    }
    
    func chooseFlow() {
        if (!self.context.authService.isAuthorized) {
            startLogin()
        } else {
            startRooms()
        }
    }
    
    func startLogin() {
        let nc = UINavigationController()
        window.changeRootViewController(nc)
        self.authCoordinator = AuthCoordinator(router: nc, context: context)
        self.authCoordinator!.completionHandler = { [weak self] in
            Log("Auth completed")
            self?.authCoordinator = nil
            self?.startRooms()
        }
        self.authCoordinator!.start()
    }
    
    func startRooms() {
        Log("Start rooms")
        let nc = UINavigationController()
        window.changeRootViewController(nc)
        self.chatRoomsCoordinator = ChatRoomsCoordinator(router: nc, context: self.context)
        self.chatRoomsCoordinator?.start()
    }
}
