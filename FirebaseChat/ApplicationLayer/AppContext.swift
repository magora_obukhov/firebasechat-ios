import Foundation
import Firebase

protocol RealtimeDatebaseContext {
    var chatService: ChatService { get set }
}

protocol AuthServiceContext {
    var authService: AuthService { get set }
}

typealias AppContext = RealtimeDatebaseContext & AuthServiceContext

open class AppContextImpl: AppContext {
    var chatService: ChatService
    var authService: AuthService
    
    init() {
        self.chatService = ChatService()
        self.authService = AuthServiceImpl()
    }
}
