import UIKit
import MAGChatUI
import IQKeyboardManagerSwift
import Firebase
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinator!
    var appContext: AppContext!
    private let disposeBag = DisposeBag()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let screenBounds = UIScreen.main.bounds
        self.window = UIWindow(frame: screenBounds)
        self.window?.makeKeyAndVisible()
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        FirebaseApp.configure()
        //
        try? Auth.auth().signOut()
        self.appContext = AppContextImpl()
        populateWithRooms()

        self.appCoordinator = AppCoordinator(window: window!, context: appContext)
        self.appCoordinator.start()
        
        return true
    }
    
    private func populateWithRooms() {
        let rooms = ["iOS Devs", "Android Devs"]
        for room in rooms {
            self.appContext.chatService.create(roomNamed: room)
                .subscribe(onSuccess: { (room) in
                    Log("Room created: \(room.name)")
                })
                .disposed(by: disposeBag)
        }
    }
}

