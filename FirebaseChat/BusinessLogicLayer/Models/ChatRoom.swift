import Foundation

struct ChatRoom {
    var id: String
    var name: String
    
    init(dto: ChatRoomDTO) {
        self.id = dto.id ?? ""
        self.name = dto.name ?? ""
    }
}
